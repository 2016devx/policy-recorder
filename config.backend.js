/**
 * 后端API配置文件
 * @date 2017-05-31
 * @author codemarker
 */
// 存储上传文件数据
var $fileMap = {};
// 服务器主机
var $$HOST = "";
var BACKEND, URL_PROVINCE, URL_CITY, QUERY_URL_LICENSE, LOGIN, LOGOUT;
if(/mock-api|localhost|127.0.0.1/.test(location.href)){
    // 测试主机地址
    $$HOST = "http://mock-api.xjt365.cn";
} else {
    // 正式主机地址
    $$HOST = "http://api.xjt365.cn";
}

BACKEND = $$HOST + '/v1/policy/sync/cardata';
// 获取省份代号
URL_PROVINCE = $$HOST + '/v1/policy/area/city';
// 获取城市代号
URL_CITY = $$HOST + '/v1/policy/area/city?code=';

// 通过车牌号查询相关信息的屏蔽接口
QUERY_CAR_LICENSE_WITH_MASK = $$HOST + '/v1/policy/query';
// 通过车牌号查询相关信息的非屏蔽接口
QUERY_CAR_LICENSE_WITH_NO_MASK = $$HOST + '/v1/policy/find';

// 查询险别
RISKLIST = $$HOST + '/v1/policy/risks';

LOGIN  = $$HOST + '/view/v1/admin/policy/login';
LOGOUT = $$HOST + '/view/v1/admin/policy/logout';

LOGIN_POST = $$HOST + '/v1/user/login';
LOGOUT_POST = $$HOST + '/v1/user/logout';
