// jQuery & Esstential Plugin
var $ = window.jQuery = window.$ =  require('./vendors/jquery');
require('./vendors/bootstrap.3.3.7');
var validateFactory = window.validateFactory = require('./mod/validate');
var alertify = window.alertify = require('./vendors/alertify');
