// var $ = document.querySelector.bind(document);

// 匹配数据格式的正则
var rTextFormat = '\n';


/**
 * 解析粘贴板文本
 *
 * @author      codemarker
 * @version     0.0.1
 * @date        2017-03-03
 */
var parseClipboardText = function( data ) {
	var arr = data.split(rTextFormat);
	return arr.map(function( item, index ){
		return item.trim();
	});
};


module.exports = parseClipboardText;