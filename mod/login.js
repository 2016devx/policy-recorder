/**
 * 登录逻辑
 * @version  0.0.1
 * @date 2017-04-28
 *
 * @todo 
 * - error数量跟踪
 * - 验证码3分钟失效检测
 * 
 */

// 页面重定向
// localtion.href = '';

jQuery(document).ready(function() {

    /*
     Fullscreen background
     */
    
    $.backstretch(__uri('../bg-member-login.jpg'));
    // $.backstretch("/bg-member-login.jpg");

    // 获取所有的验证规则
    var rules = validateFactory.rules;

    /*
     Form validation
     */
    var $loginFormContainer = $('.login-form');
    var $submitBtn          = $('#J_submitBtn')

    var STATE_SUCCESS   = 'has-success';
    var STATE_ERROR     = 'has-error';
    var FEEDBACK_OK     = 'glyphicon-ok';
    var FEEDBACK_ERROR  = 'glyphicon-remove';

    // State Flag
    var checkingState = {
        username  : false,
        password  : false,
        verifyCode: false
    };

    var $username          = $('#form-username');
    var $usernameWrapper   = $username.closest('div.has-feedback');
    var $usernameFeedbackState = $username.next('span.form-control-feedback');

    var $password          = $('#form-password');
    var $passwordWrapper   = $password.closest('div.has-feedback');
    var $passwordFeedbackState = $password.next('span.form-control-feedback');

    var $verifyCode = $('#form-verifyCode');
    var $verifyCodeWrapper   = $verifyCode.closest('div.has-feedback');
    var $verifyCodeFeedbackState = $verifyCode.next('span.form-control-feedback');

    function inputEmptyStateChecking (inputItem, inputWrapper, feedbackState) {
        // debugger;
        var inputValue = $.trim(inputItem.val());
        // console.log(inputValue);

        if(inputValue === '') {
            inputWrapper[0].className = 'form-group has-feedback ' + STATE_ERROR;
            feedbackState[0].className = 'glyphicon form-control-feedback ' + FEEDBACK_ERROR;
            return false;
        } else {
            inputWrapper[0].className = 'form-group has-feedback ' + STATE_SUCCESS;
            feedbackState[0].className = 'glyphicon form-control-feedback ' + FEEDBACK_OK;
            return true;
        }
    }

    $username.on('blur', function() {
        checkingState.username = inputEmptyStateChecking($username, $usernameWrapper, $usernameFeedbackState);
    });

    $password.on('blur', function() {
        checkingState.password = inputEmptyStateChecking($password, $passwordWrapper, $passwordFeedbackState);
    });

    $verifyCode.on('blur', function() {
        checkingState.verifyCode = inputEmptyStateChecking($verifyCode, $verifyCodeWrapper, $verifyCodeFeedbackState);
    });

    var $changeVerifyCodeEl = $('#J-changeVerifyCode');
    var $refreshVerifyCodeEl = $('#J-refreshVerifyCode');

    // 避免浏览器缓存
    function changeVerifyCodeImage() {
        $changeVerifyCodeEl.attr('src', 'http://api.xjt365.cn/v1/validator/imgcode' + '?v=' + new Date().getTime())
    }

    $changeVerifyCodeEl.on('click', changeVerifyCodeImage);
    $refreshVerifyCodeEl.on('click', changeVerifyCodeImage);

    function buildDataObject(sendtime, data) {
        var obj = {
            "request": {
                "head": {
                    "acount": "",
                    "key": "",
                    "sendtime": "",
                    "transactionid": "",
                    "sign": "",
                    "callback": "commit",
                    "custom": ""
                },
                "body": {

                }
            }
        };

        obj["request"]["head"]["sendtime"] = sendtime;
        obj["request"]["body"]     = data;

        return obj;
    }


    $submitBtn.on('click', function(e) {
        e.preventDefault();

        // 提交前触发blur状态
        $username.trigger('blur');        
        $password.trigger('blur');        
        $verifyCode.trigger('blur');        

        
        for(var key in checkingState) {
            // 状态监控
            console.log(key, checkingState[key]);

            if(checkingState.hasOwnProperty(key)) {
                if(checkingState[key] !== true) {
                    return;
                }
            }
        }

            // @todo
        var postData = {
            "username": $username.val(),
            "password": $password.val(),
            "imgcode" : $verifyCode.val()
        };

        var data = buildDataObject(
            moment().format('YYYY-MM-DD HH:mm:ss'),
            postData
            );

        console.log(data);

        $.ajax({
            url: LOGIN_POST,
            type: 'POST',
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(data)
        }).done(function (res) {
            var head = res.response.head;
            if(head.errcode === '00000') {
                alertify.success('登录成功!');
                setTimeout(function(){
                    location.href = './index';
                }, 200);
            } else {
                // 显示后端返回的错误信息
                if(head && head.message) {
                    alertify.error(head.message[0].validate);
                } else {
                    alertify.error('登录出错!');
                }

            }
        });

    });


});
