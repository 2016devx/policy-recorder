/**
 * 规范化后端返回的数据,比如深圳的车险数据
 * @date 2014-06-09
 * @author codemarker
 */
function normalizeDate(dateString){
    var s = jQuery.trim(dateString);
    var result = s.match(/(\d+)年\s*(\d+)月\s*(\d+)日/);
    var d = ''; // 最终返回的正确日期数据形式
    var year, month, day;
    if(result === null) {
        d = dateString
    } else {
        year  = result[1];
        month = result[2].length === 1 ? '0' + result[2] : result[2];
        day   = result[3].length === 1 ? '0' + result[3] : result[3];
        d = year + '-' + month + '-' + day;
    }
    return d;
}


module.exports = normalizeDate;

