// Deps
// var $ = require('../vendors/jquery');

/**
 * @todo
 * 定义一个Validate类,接收options参数
 * 整合BootStrap表单校验状态(has-success, has-error等)
 * validate()方法支持传入一系列对象
 * rules支持数组传入, 字符串的形式内部被转换为包含单个元素的数组
 */

;(function(){



/**
 * ===========
 * 数组辅助方法
 * ===========
 */
/**
 * Remove an item from an array
 * @ref 参考Vue的$remove实现
 */
var remove = function(arr, item) {
    if (arr.length) {
        // 获取item的索引值
        var index = arr.indexOf(item);
        if (index > -1) {
            return arr.splice(index, 1);
        }
    }
};

var indexOf = function(arr, item) {
    return arr.indexOf(item);
};

var inArray = function(arr, item) {
    return !!~arr.indexOf(arr, item)
};


//用于显示错误提示
//HTML 格式 <div class="l tips"><em id="err-intro"></em></div>
//@param id 元素的CSS表达式
//@param status
//0 表示失败，这时第三个参数有效，显示这红色字
//1 表示成功，会在此元素的父节点上添加一个叫okey的类名，显示绿色的勾号
//2 表示隐藏，去掉元素的innerHTML与父节点上的okey的类名

//@param msg 错误消息

// @todo options接收validatedSuccessfulClassName:
// @example has-error, hassuccess


// var parentSelector = '.form-group';

var showTip = window.showTip = function (id, status, msg){
    var
        el = $(id),
        parent = el.parent(),
        node = el[0],
        nodes = showTip.nodes
    ;

    switch(status){
        case 0 :
            parent.removeClass('has-success');
            parent.addClass('has-error');

            if(node){
                node.innerHTML = msg;
                if(inArray(nodes, node)){//用去统计当前页面有多少个验证没有被通过
                    nodes.push(node);
                }
            }
            break;

        case 1:
            parent.removeClass('has-error');
            parent.addClass('has-success');
            if(node){
                // node.innerHTML = "";
                remove(nodes, node);
            }
            break;

        case 2:
            parent.removeClass('has-success');
            if(node){
                // node.innerHTML = "";
                remove(nodes, node);
            }
            break;
    }
}

showTip.nodes = [];


/**
 * 验证器核心实现逻辑
 *
 *@param root 绑定事件的元素的CSS选择器，通常是form元素
 *@param name 控件的类名，要去掉前面的点号。之所以用类名，因为checkbox是一组的，共用一个name值，不能用ID
 *@param obj 验证用的函数与错误提示，错误提示作为键名，函数为值。
 *@param checktype 触发验证用的事件名，默认为blur
 *
 * @update
 * 实现为jQuery事件绑定的方式
 */
function validate(root, name, obj, checktype){
    // 对象失去焦点时触发
    checktype = checktype || "blur";

    //@todo 提取出来
    var $rootElement = $(root);

    $rootElement.on(checktype, "." + name, function(){
        var ok = true;
        for(var msg in obj){
            if(!obj[ msg ](this)){
                showTip("#err-" + name, 0 , msg );//失败了就显示红色的错误提示
                $("#err-" + name).removeClass('help-hidden');
                $("#err-" + name).addClass('help-visible');

                ok = false;
                break;
            }
        }
        if(ok){
            $("#err-" + name).removeClass('help-visible');
            showTip("#err-" + name, 1);//显示成功提示，绿色的勾号
        }
    }).on("focus", "." + name, function(){
        // showTip("#err-" + name, 2);//隐藏所有提示！
        $("#err-" + name).addClass('help-hidden');//隐藏所有提示！
    });
}




/*
 * 根据手机号码获取运营商的序号
 * @param { Number } 11位手机号码
 * @return { Number } 各运营商对应的序号
 * 0 : 移动，1 : 联通，2 ： 电信，-1 : 号码错误
 */
var getISP = function( number ){
    var rCMCC = /^(139|138|137|136|135|134|159|158|152|151|150|157|188|187|147|182|183)[0-9]{8}$/,  // 移动
        rUNICOM = /^(130|131|132|155|156|185|186)[0-9]{8}$/,    // 联通
        rCT = /^(133|153|180|189)[0-9]{8}$/;    // 电信

    return rCMCC.test(number) ? 0 :
        rUNICOM.test(number) ? 1 :
            rCT.test(number) ? 2 :
                -1;
};


// 验证规则
// 策略对象,封装具体的算法实现
var rules = {
    noempty: {
        "不能为空" : function(el){
            return $.trim(el.value).length !== 0;
        }
    },


    number: {
        "只能填一个正整数" : function(el){
            var i = el.value;
            return Number(i) > 0 && parseInt(i) === Number(i);
        }
    },


    email: {
        "格式不正确":function(el){
            var val = $.trim(el.value);
            return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(val);
        }
    },


    phone: {
        "格式不正确":function(el){
            var val = $.trim(el.value);
            // @note
            // 暂时验证11位就够了
            return val.length !== 11;
        }
    },

    // 参考:
    // http://www.blogjava.net/weiwei/articles/401703.html
    // 车牌号
    licenseNo: {
        "格式不正确": function (el) {
            var val = $.trim(el.value);
            return /(^[\u4E00-\u9FA5]{1}[A-Z0-9]{6}$)|(^[A-Z]{2}[A-Z0-9]{2}[A-Z0-9\u4E00-\u9FA5]{1}[A-Z0-9]{4}$)|(^[\u4E00-\u9FA5]{1}[A-Z0-9]{5}[挂学警军港澳]{1}$)|(^[A-Z]{2}[0-9]{5}$)|(^(08|38){1}[A-Z0-9]{4}[A-Z0-9挂学警军港澳]{1}$)/.test(val);
        }
    },

    // [15/18位身份证号码正则表达式（详细版）](http://blog.csdn.net/wei549434510/article/details/50596207)
    idno: {
        "格式不正确": function (el) {
            var val = $.trim(el.value);
            return /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)/.test(val);
        }
    }

};


// @todo 放在原型上
var addRule = function(key, value) {
    rules[key] = value;
};


// Expose...
window.validateFactory = {
    showTip: showTip,
    validate: validate,
    addRule: addRule,
    rules: rules
};

})();