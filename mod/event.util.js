/**
 * addEvent Helper
 * @pattern 惰性加载模式
 */
var addEvent = (function(){
    if (window.addEventListener) {
        addEvent = function(element, type, handler) {
            element.addEventListener(type, handler, false);
        };
    } else if (window.attachEvent) {
        addEvent = function(element, type, handler) {
            element.attachEvent("on" + type, handler);
        };
    }
    return addEvent;
})();

/**
 * 事件辅助函数
 *
 * @author  codemarker
 * @date 2017-03-03
 * @ref
 * http://blog.csdn.net/allen_hdh/article/details/20746315
 */
var EventUtil = {
    addHandler: addEvent,


    getEvent: function (event) {
        return event ? event : window.event;
    },


    getClipboardText: function (event) {
        var clipboardData = (event.clipboardData || window.clipboardData);
        return clipboardData.getData("text");
    },


    setClipboardText: function (event, value) {
        if (event.clipboardData) {
            return event.clipboardData.setData("text/plain", value);
        } else if (window.clipboardData) {
            return window.clipboardData.setData("text", value);
        }
    },


    preventDefault: function (event) {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    }
};


module.exports = EventUtil;