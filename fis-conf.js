/**
 * Baidu FIS3配置文件
 */

fis.media('dp').match('*.{jpg,jpeg,gif,png,js,css}', {
  release: '/resources/publicd/x/$0',
  useHash: true // [文件指纹](http://fis.baidu.com/fis3/docs/beginning/release.html#%E6%96%87%E4%BB%B6%E6%8C%87%E7%BA%B9)
});


fis.media('dp').match('*.html', {
  optimizer: fis.plugin('html',{
    option: {
      removeAttributeQuotes: true,
      collapseWhitespace: true,
      removeComments: true
    }
  })
});




fis.media('dp').match('*.js', {
  optimizer: fis.plugin('uglify-js')
});


/**
 * Deploy & 文件过滤
 */
fis.media('dp').match('*', {
	deploy: [
		fis.plugin('filter', {
			exclude: [
				'Gruntfile.js',
				'fis-conf.js',
				'code_snippet.js',
				'webupload.entry.js',
				'doc.md',
				'todo.md',
				'config.rb',
				'package.json',
				'fileupload.php',
				'backend.php',
				'debug_snippet.js',
				'styles/**.*',
				'demo/**.*',
				'data/**.*',
				'mod/**.*',
				'vendors/**.*',
                'README.md',
				// Ignore all handlebars template
                '**.handlebars'
			]
		}),


		fis.plugin('local-deliver', {
			to: '../fis3-deploy'
		}),


		// zip打包
		// fis.plugin('zip', {
		// 	filename: 'fis3-deploy.pack.zip'
		// })
	]
});
