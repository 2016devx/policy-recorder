# 开发人员使用文档

## 安装FIS3
> 确认当前终端是否可以执行`fis3`
``` bash
fis3 --version
```

否则通过以下命令切换到Node V7.2.1
``` bash
# 设置全局Node版本为7.2.1
nodist 7.2.1

# 安装[fis3](http://fis.baidu.com/)作为前端构建工具
npm i -g fis3
```

## 开启开发服务器

`` bash
# 步骤1
fis3 server start
```

## 开启文件监听服务& 浏览器自动刷新

``` bash
# 步骤2
fis3 release -wL
```


## 执行Grunt任务运行期

``` bash
# 步骤3
grunt
```

## 车牌号
测试账号: *粤A2MH73*
查询账号: *粤ANB111*
深圳测试账号: *粤BZC143*


## 删除的npm scripts
``` bash
# package.json
"watch": "watchify app.js -d -o bundle.js -v",
```
