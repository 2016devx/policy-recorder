/**
 * 保单录入程序 
 * @version 0.0.1
 * @date 2017-04-27
 *
 *
 * @todo 
 * 调整为grunt concat模式
 * 但是,watchify&webpack不支持单文件调试
 * What The Fuck?
 *
 * Fixed表单验证页面滚动问题
 */

require('./vendors/bootstrap-datetimepicker');
require('./vendors/locales/bootstrap-datetimepicker.zh-CN');
require('./vendors/jquery.auto-complete');

var normalizeDate = require('./mod/normalizeDate');

var hasOwn = Object.prototype.hasOwnProperty;

var clearVM = function (vm) {
    for(var key in vm._data)     {
        // 避免原型继承过来的属性
        if(hasOwn.call(vm._data, key)) {
            // 属性字段包含date的就保留,否则重置掉
            if(!~key.indexOf('date')) {
                vm[key] = '';
            }
        }
    }
};


var resetRiskList = function(){
    var $risklist = $('#J-risklist');
    $risklist.find('input[type=checkbox]').attr('checked', false);
    $risklist.find('input[type=radio]').attr('checked', false);
};

/**
 * 将数据源对象绑定到ViewModel对象中
 * @author codemarker
 * @date 2017-06-06 10:21
 * @param vm {Object} ViewModel对象
 * @param data {Object} DataSource对象
 */
var dataBindToVM = (function(vm, data) {
    var
        keys    = Object.keys,
        forEach = Array.prototype.forEach
    ;


    if(!!keys && !!forEach) { // For Modern Browser
        return function __dataBindToVM__ (vm, data) {
            Object.keys(data).forEach(function(key) {
                vm[key] = data[key];
            });
        };
    } else { // For IE
        return function __dataBindToVM__ (vm, data){
            for(var key in data)     {
                // 避免原型继承过来的属性
                if(hasOwn.call(data, key)) {
                    vm[key] = data[key];
                }
            }
        }
    }
})();


// 用于缓存资源请求,减轻服务器压力
var requestCache = {};


var DEBUG;
// // 对应于线上部署环境
DEBUG = false;
//
// // 对应于线下调试环境
// DEBUG = true;


var debug_data = {
    // **** 1. 基本信息 ****
    citycode  : '0000', // 城市代码

    // licenseno : '粤ANB111', // 车牌号

    licenseno : '', // 车牌号

    driveCity : '广东省广州市', // 行驶城市,

    // sameAsDriveCity: true, // 默认与车辆行驶城市相同

    // **** 2. 车主信息 ****
    ownername : '张三',

    // 提交时实质上上是identity字段
    idno      : '440782199001011111',

    // sex      : '男', /* 默认值 */
    // address  : '珠江新城',
    // 11个0
    phone : '00000000000',

    contact_addr: '',

    // **** 3. 车辆信息 ****
    // 车架号
    frameno   : 'LSVCF6A42BN190323',

    engineno  : 'CFM', // 发动机型号

    // 车辆品牌
    carbrand  : '大众',

    // 注册登记日期
    registdate: '2017-01-01',

    tci_enddate: '2017-01-01 00:00:00',

    vci_enddate: '2017-01-01 00:00:00',

    preinsureinfo: '',

    // 保单日期
    policydate: '2017-01-01',

    type: 'P0',

    // 保险公司的ID , 从policycmp获取
    cmpid: '',

    id: '',

    uris: [
    ]
};


var dev_data = {
    // **** 1. 基本信息 ****
    citycode  : '', // 城市代码

    licenseno : '', // 车牌号

    driveCity : '', // 行驶城市,

    // sameAsDriveCity: true, // 默认与车辆行驶城市相同

    // **** 2. 车主信息 ****
    ownername : '',

    // 提交时实质上上是identity字段
    idno      : '',

    // sex      : '男', /* 默认值 */
    // address  : '珠江新城',
    phone : '00000000000',

    contact_addr: '',

    // **** 3. 车辆信息 ****
    // 车架号
    frameno   : '',

    engineno  : '', // 发动机型号

    // 车辆品牌
    carbrand  : '',

    // 注册登记日期
    registdate: '2017-01-01',

    tci_enddate: '2017-01-01 00:00:00',

    vci_enddate: '2017-01-01 00:00:00',

    preinsureinfo: '',

    // 保单日期
    policydate: '2017-01-01',

    type: 'P0',

    cmpid: '',

    // 用户标识系统是更新车辆操作
    id: '',

    uris: [
    ]

};


// Vue实例(ViewModel)
var vm = window.vm = new Vue({
    el: '#f-app',

    // 模拟数据
    data: DEBUG ? debug_data : dev_data,


    // https://cn.vuejs.org/v2/api/#computed
    computed: {
        insured_area: function(){
            return this.driveCity;
        },

        // 移除已经上传的保单图片
        removeUploadedImage: function(){
            // @todo
        }
    }
});


// DOM ready时触发
jQuery(document).ready(function($){
    var $rootSelector = '#J_form';

    // 获取所有的验证规则
    var rules = validateFactory.rules;


    // 车牌号
    // validateFactory.validate(
    //     $rootSelector,
    //     'f-licenseno',
    //     rules.noempty
    // );

    validateFactory.validate(
        $rootSelector,
        'f-licenseno',
        rules.licenseNo
    );

    // 用户名
    validateFactory.validate(
        $rootSelector,
        'f-ownername',
        rules.noempty
    );

    // // 手机号码
    // validateFactory.validate(
    //     $rootSelector,
    //     'f-telephone',
    //     rules.noempty
    // );

    // validateFactory.validate(
    //     $rootSelector,
    //     'f-telephone',
    //     rules.phone
    // );

    // // 身份证
    // validateFactory.validate(
    //     $rootSelector,
    //     'f-idno',
    //     rules.noempty
    // );

    // // 验证身份证的正则
    // validateFactory.validate(
    //     $rootSelector,
    //     'f-idno',
    //     rules.idno
    // );


    // 车架号
    validateFactory.validate(
        $rootSelector,
        'f-frameno',
        rules.noempty
    );

    // 发动机型号
    validateFactory.validate(
        $rootSelector,
        'f-engineno',
        rules.noempty
    );


    // 车辆品牌
    validateFactory.validate(
        $rootSelector,
        'f-carbrand',
        rules.noempty
    );


    // 注册登记日期
    validateFactory.validate(
        $rootSelector,
        'f-registration_date',
        rules.noempty
    );

    

    /** 
     * 城市选择器相关代码
     *
     */
    var $provinceContainer = $('#J_driveProvince');
    // var $provinceOptions   = $provinceContainer.find('option');

    var $cityContainer     = $('#J_driveCity');
    // var $cityOptions       = $cityContainer.find('option');

    // 省份代码,接口需要这个来获取城市代码
    var province_code      = '00';

    /**
     * 存储行驶城市相关信息,比如省份,城市
     *
     * 结构:
     * {
     *     province: '',
     *     city: ''
     * }
     */
    var driveCityInfo      = {
        province: '',
        city    : ''
    };


    function renderOptionItems(data) {
        var retArr = [];
        var d = data.response.body.data.list;

        // 避免repaint
        $.each(d, function(index, item){
            // @date 2017-06-02
            // 后台返回字段由city_code调整为code
            retArr.push('<option value=' + item.code +'>' + item.title +'</option>');
        });
        return retArr.join('');
    }

    // Ajax请求加载省份列表
    $.ajax({
        url: URL_PROVINCE,
        success: function __handleProvinceData(data){
            var htmlString = renderOptionItems(data);
            $provinceContainer.html(htmlString);
        },
        error: function __handleAjaxCityError(err) {
            alertify.error('内部服务中断,请重新加载页面看看');
        }
    });


    $provinceContainer.on('change', function(event){
        province_code = this.value;

        // 获取城市代号
        console.log('城市id:' + province_code);


        // @ref
        // [JS获取表单选中项](http://blog.csdn.net/foart/article/details/6614829/)
        var province_name = $(this).find('option').filter(':selected').text();
        driveCityInfo.province = province_name;
        console.log("所选择的省份名称: " + province_name);

        // @todo
        // 可能将会会存储数据
        // - 存储编译字符串
        if(typeof requestCache[province_code] === 'undefined') {
            var provinceDataObject = requestCache[province_code] = {};
            
            // Reset
            $cityContainer.html('');

            // Ajax请求加载城市列表
            $.ajax({
                url: URL_CITY + province_code,
                success:  function __handleCityData(data){
                    var htmlString = renderOptionItems(data);
                    $cityContainer.html(htmlString);

                    var $cityOption = $cityContainer.find('option');
                    var $firstCityOption = $cityOption.eq(0).attr('selected', true);

                    var city_value = $cityContainer.val();
                    console.log('下拉列表中第一个城市名称为:' + city_value);

                    vm.citycode = city_value;

                    var city_name = $cityOption.filter(':selected').text();

                    console.log('城市名称: ' + city_name);

                    driveCityInfo.city = city_name;

                    vm.driveCity = driveCityInfo.province + driveCityInfo.city;


                    // 设置请求过URL的状态
                    provinceDataObject['data'] = data;
                    provinceDataObject['compiledHTML'] = htmlString;
                }
            });
        } else {
             // Reset
            $cityContainer.html('');
            
            var compiledHTML = requestCache[province_code]['compiledHTML'];
            $cityContainer.html(compiledHTML);
        }
    });


    $cityContainer.on('change', function(event){
        console.log('citycode:', this.value);
        vm.citycode = this.value;

        var city_name = $(this).find('option').filter(':selected').text();

        driveCityInfo.city = city_name;
        console.log("城市名称: " + city_name);
        vm.driveCity = driveCityInfo.province + driveCityInfo.city;
    });



    /*
     * 请求体
     */
    var licenseConfirmObj = {
        "request": {
            "head": {
                "acount": "必传",
                "key": "必传",
                "sendtime": "必传",
                "transactionid": "必传",
                "sign": "必传",
                "callback": "必传",
                "custom": "必传"
            },
            "body": {
                "licenseno": ''
            }
        }
    };


    // 监听表单项的blur事件验证车牌号是否输入过
    $('#J_licenseno').on('blur', function __licensenoBlur__(){
        var val = licenseConfirmObj.request.body.licenseno = this.value;

        // 因为提前作为车牌号正则验证
        // 正则验证失败就立即返回
        var className = 'has-error';
        if(!!~this.parentNode.className.indexOf(className)) {
            return;
        }

        // console.log(val);

        $.ajax({
            url: QUERY_CAR_LICENSE_WITH_NO_MASK,
            type:'POST',
            processData: false,
            contentType: false,
            data: JSON.stringify(licenseConfirmObj)
        }).then(function __queryCarHasInput__(res){
            var response = res.response;

            // 避免Error
            if(response &&
            response.body &&
            response.body.data &&
            response.body.data.list) {

                if(response.body.data.list.length !== 0) {
                    alertify.success('你已经输入过该车辆数据');
                    alertify.success('系统正为你填充已提交的数据');

                    // 默认取第一个数据即可
                    var temp1 = response.body.data.list[0];

                    // 只要险别类型(kindcodes)存在的时候才重新编译
                    if(temp1 && temp1.kindcodes) {

                        // 解析后端返回的字符串数据对象结构
                        var obj = JSON.parse(temp1.kindcodes);


                        /**
                         * 修正保单类型find接口不能同步的问题
                         * @date 2017-06-15
                         */
                        vm.type = obj.type;


                        /**
                         * 自动绑定险别列表数据
                         * @version 1.0.0
                         * @date 2017-06-09
                         */
                        var arr = [
                            'mainrisks',
                            'additionalrisk',
                            'regardlessrisk'
                        ];


                        $.each(arr, function($$index, value){
                            $.each(obj[value]['list'], function(index, item){
                                console.log(index, item);
                                if(item.selected === true) {
                                    var $el = $('[data-kindcode="' + item.kindcode + '"]');
                                    var inputEl = $el[0];
                                    inputEl.checked = true;


                                    // 用于存储我们关注的select或者input元素
                                    var $target = null,

                                        // 找到第三个tr
                                        $targetContainer = $el.parent().next('td').next('td');

                                    if(item.needinput == true) {
                                        // input输入框绑定功能实现完成
                                        $target = $targetContainer.find('input').eq(0);
                                        $target.val(item.amount);
                                    } else {
                                        $target = $targetContainer.find('select');
                                        if($target.length > 0) {
                                            var $$optionArr = $target.eq(0).find('option').filter(function(index, item){
                                                return item.innerText === item.amount;
                                            });

                                            var $$optionTarget = $$optionArr.eq(0);

                                            $$optionArr.attr({
                                                "selected": true
                                            });

                                            $target.eq(0).val(item.amount);
                                        }
                                    }
                                }
                            });

                        });


                        var policycmp = obj.policycmp;

                        try {
                            if(policycmp && policycmp[0]) {
                                var $$policycmp = $('[data-id="' +  policycmp[0].id +'"]');
                                var policycmpNode = $$policycmp[0];
                                console.log($$policycmp);
                                policycmpNode.checked = !policycmpNode.checked;
                            }
                        } catch(e) {
                            alertify.warning('请选择保单所属的保险公司!');
                        }



                    }

                    dataBindToVM(vm, temp1);

                    console.log(temp1.registdate);
                    console.log(normalizeDate(temp1.registdate));

                    // 调整返回的注册等级日期
                    vm.registdate = normalizeDate(temp1.registdate);

                    // @todo
                    // - 后期调整vm字段
                    vm.idno = temp1.identity;

                    // console.log('imguri:', temp1.imguri);

                    try {
                        var imgUriArr = [];

                        $.each(JSON.parse(temp1.imguri) , function(idx, value){
                           imgUriArr.push($$HOST + '/' + value);
                        });

                        vm.uris = imgUriArr;
                    } catch(e){
                        console.log('图片上传数据结构有问题,应当为数组类型');
                    }

                    delete temp1;
                }
            } else {
                var messageArr = response.message;
                if(messageArr && messageArr[0]) {
                    // alertify.warning('此车牌号' + messageArr[0].validate);
                    alertify.warning(messageArr[0].validate);
                }
            }
        });
    });

    

    /**
     * @note
     * 封装以实现方法重用
     */
    function changeDatetime($el, option) {
        var datetimepickerOptions = {
            language: 'zh-CN',
            minView : 1,
            maxView : 4,
            format  : 'yyyy-mm-dd'
        };

        var option = $.extend({}, datetimepickerOptions, option);
        /**
         * @Ref [DatePicker事件使用](http://www.bootcss.com/p/bootstrap-datetimepicker/#events)
         */
        return $el.datetimepicker(option);
    }

    var $registDateElem = $('#J_registration_date');
    var $tciEndDateElem = $('#J_tci_enddate');
    var $vciEndDateElem = $('#J_vci_enddate');

    changeDatetime($registDateElem)
        .on('hide', function (event) {
            // 当用户更改日期,触发Vue更改模型
            var val = event.target.value;
            vm.registdate = val;
        });

    changeDatetime($tciEndDateElem, {
        format: 'yyyy-mm-dd HH:mm:ss'
    }).on('hide', function(event){
        // 修正因为粘贴的问题导致日期显示不正常的问题
        var val = event.target.value;
        event.target.value = val.replace(/\d{2}\:\d{2}:\d{2}/, '23:59:59');
        vm.tci_enddate = event.target.value;
    });

    changeDatetime($vciEndDateElem, {
        format: 'yyyy-mm-dd HH:mm:ss'
    }).on('hide', function (event) {
        // 修正因为粘贴的问题导致日期显示不正常的问题
        var val = event.target.value;
        event.target.value = val.replace(/\d{2}\:\d{2}:\d{2}/, '23:59:59');
        vm.vci_enddate = event.target.value;
    });



    // // 保单日期
    // changeDatetime('#J_policydate',       'policydate', {
    //     format: 'yyyy-mm-dd'
    // });

    /**
     * 险别列表请求头信息
     */
    var riskRequestInfo = {
        "request": {
            "head": {
                "acount": "必传",
                "key": "必传",
                "sendtime": "必传",
                "transactionid": "必传",
                "sign": "必传",
                "callback": "必传",
                "custom": "必传"
            },
            "body": {
            }
        }
    };

    $.ajax({
        url: RISKLIST,
        type: 'POST',
        data:  JSON.stringify(riskRequestInfo),
        dataType: 'json',
        contentType: 'application/json',
        processData: false
    }).then(function(res) {
        var head = res.response.head;
        var data = null;
        if(head && head.errcode === '00000') {
            // 请求成功
            data = res.response.body.data;


            // @todo
            // - remove
            console.log(data);


            var $riskList = $('#J-risklist');

            var s = Handlebars.templates.risklist(data);

            $riskList.html(s);


            var $tableSection = $riskList.find('.table-section');

            /**
             * 优化点击checkbox的使用体验
             * @date 2017-05-31 11:19
             * @author codemarker
             *
             * @update 2017-06-05
             * - 避免点击输入框或者弹出下拉菜单的时候更改checkbox的状态,从而优化用户体验
             */
            $tableSection.on('click', 'tr', function(event){
                if(event.target && event.target.nodeName === 'INPUT' || event.target.nodeName === 'SELECT') {
                    return;
                }

                var $checkbox = null;
                var $radio    = null;
                var $context  = $(this);

                $checkbox = $context.find('input[type=checkbox]');
                if($checkbox.length > 0) {
                    var checkbox = $checkbox[0];
                    // 取回checkbox的DOM元素
                    var toggleStatus = checkbox.checked;
                    checkbox.checked = !toggleStatus;
                } else {
                    $radio = $context.find('input[type=radio]');
                    var radio = $radio[0];
                    // 取回radio的DOM元素
                    var toggleStatus = radio.checked;
                    radio.checked = !toggleStatus;
                }

            });
        }
    });



    /**
     * 获取保险公司信息
     * @return {Object}
     */
    function getPolicyCompanyInfo() {
        var o = {};

        var $policycmp = $('#J-policycmpList');
        var $policycmpRadios = $policycmp.find('input[type=radio]');
        var $policycmpRadioOfChecked = $policycmpRadios.filter(':checked');
        var size = $policycmpRadioOfChecked.length;

        var $el = null;

        // @todo
        if(size > 0) {
            // 获取选中的元素
            $el = $policycmpRadioOfChecked.eq(0);
            o.id = $el.data('id');
            o.name = $el.data('name');
            // 设置selected状态,方便模板重新渲染
            o.selected = true;
        }

        return o;
    }

    /**
     * 收集险别checkbox输入项的信息
     * @return arr
     */
    function riskObjectFactory($context) {

        // 获取所有的checkbox输入项
        var $riskElemsOfCheckbox = $context.find('input[type=checkbox]');

        // console.log($riskElemsOfCheckbox);
        var riskCheckboxArr = $.map($riskElemsOfCheckbox, function(item, index){
            var $item = $(item),
                arr = [],
                needinput = false,
                amount = null,
                $emptyOrSelectInput = $item.parent().next().next(),
                riskName = $item.data('name'),
                kindcode = $item.data('kindcode')
            ;

            // if(kindcode === 'V0007') {
            //     debugger;
            // }

            var $select = $emptyOrSelectInput.find('select');
            var $input  = $emptyOrSelectInput.find('input');
            if($select.length !== 0) {
                amount = $select.eq(0).val();
            }

            if($input.length !== 0) {
                amount = $input.eq(0).val();
                // 设置needinput的状态
                needinput = true;
            }


            // 记录checked状态
            arr.push({
                needinput: needinput,
                name: riskName,
                kindcode: kindcode,
                amount: amount,
                selected: item.checked ? true : false
            });
            return arr;
        });

        //     debugger;

        return riskCheckboxArr;
    }


    /**
     * 收集险别信息
     * @date 2017-05-31
     * @author codemarker
     */
    function collectRiskInfo() {
        // 预先提供数据结构,优化性能
        // 最终作为表单上传详情中formData中kindcode字段
        // 因此我们需要将这个字段返回出来
        var obj = {
            policydate: '',
            type: '', // 取值P0(询价), P1(出单)
            mainrisks: {
                vcode: '',
                category: '',
                list: null
            },
            additionalrisk: {
                vcode: '',
                category: '',
                list: null
            },
            regardlessrisk: {
                vcode: '',
                category: '',
                list: null
            },
            // 存储保险公司相关信息
            policycmp: [
                // {
                //     id: '',
                //     name: ''
                // }
            ]
        };


        // Vue实现的双向数据绑定,直接提取就好
        obj.type = vm.type;
        obj.policydate = vm.policydate;

        var $tableSection = $('.table-section');

        $.each($tableSection, function(item, index){
            // 过滤掉保险公司部分
            if(item.id === '#J-policycmpList') {
                return;
            }

            var $this = $(this);
            // 获取险别分类,比如主险别, 附加险等等
            var key = $this.data('key');
            // 获取VCode
            var vcode = $this.data('vcode');
            // 获取Category
            var category = $this.data('category');

            var k = obj[key];
            // 查找当前section下的所有表单项,并返回一个对象信息

            k['vcode'] = vcode;
            k['category'] = category;
            k['list'] = riskObjectFactory($this);

        });

        // push保单所属的保险公司过去
        var o = getPolicyCompanyInfo();

        obj.policycmp.push(o);

        return obj;
    }




    /**
     * 保单核查相关逻辑
     * @type {[type]}
     */
    var $insuranceCheckingModal        = $('#J-insuranceCheckingModal');
    var $insuranceInfoContainer        = $('#J-insuranceInfoContainer');
    var $uploadInsuranceDetailButton   = $('#J-insuranceCheckingModal');



    function submitForm (event) {
        // event.preventDefault();
        // 提交表单时进行部分数据调整
        vm.insured_area = $('#J_insured_area').val();


        var data = [];
        data.push('<li>车主姓名:     ' 　　　+ vm.ownername + '</li>');
        data.push('<li>联系人手机:   ' 　　　+ vm.phone + '</li>');
        data.push('<li>车牌号  :     ' 　　　+ vm.licenseno + '</li>');
        data.push('<li>车架号  :     ' 　　　+ vm.frameno + '</li>');
        data.push('<li>发动机号:     ' 　　　+ vm.engineno + '</li>');
        data.push('<li>车辆品牌:     ' 　　　+ vm.carbrand + '</li>');
        data.push('<li>注册登记日期: ' 　　　+ vm.registdate + '</li>');

        
        data.push('<li>身份证号:     ' 　　　+ vm.idno + '</li>');
        data.push('<li>行驶城市:   ' 　　    + vm.driveCity + '(城市代号: ' + vm.citycode + ')</li>');
        data.push('<li>联系地址:     ' 　　　+ vm.contact_addr + '</li>');
        data.push('<li>交强险到期时间:     ' + vm.tci_enddate + '</li>');
        data.push('<li>商业险到期时间:     ' + vm.vci_enddate + '</li>');
        data.push('<li>往年投保信息:     ' 　+ vm.preinsureinfo + '</li>');

        $insuranceInfoContainer.html();


        alertify.confirm(
            '保单核查', 
            data.join(''), 
            function __sucessAlert__(){ 
                // alertify.success('努力上传中, 请稍等片刻...');
                uploadInsuranceDetail();
            }, 
            function __errorAlert__(){ 
                alertify.error('亲,要认真填写哦...');
            }
        ).set('labels', {
            ok: '确定上传',
            cancel: '重新修改'
        });  
    }

    /**
     * 上传保单详情
     *
     * @fixed-date 2017-05-05 14:10
     * @fixed 更好的错误提示
     */
    function uploadInsuranceDetail() {
        var formData = new FormData();

        formData.append('carbrand',            vm.carbrand);
        formData.append('ownername',           vm.ownername);
        formData.append('licenseno',           vm.licenseno);
        formData.append('frameno',             vm.frameno);
        formData.append('engineno',            vm.engineno);
        formData.append('citycode',            vm.citycode);

        formData.append('registdate',          vm.registdate);


        // 可选项
        formData.append('phone',               vm.phone);
        formData.append('identity',            vm.idno);
        formData.append('insured_area',        vm.insured_area);

        formData.append('contact_addr',        vm.contact_addr);
        formData.append('tci_enddate',         vm.tci_enddate);
        formData.append('vci_enddate',         vm.vci_enddate);
        formData.append('preinsureinfo',       vm.preinsureinfo);

        for(var i in $fileMap) {
            formData.append(i, $fileMap[i]);
        }


        var kindcode = collectRiskInfo();
        console.log(kindcode);


        formData.append('kindcodes', JSON.stringify(kindcode));

        if(vm.id !== '') {
            formData.append('id', vm.id);
        }

        // @ref 
        // [jQuery.ajax](http://www.w3school.com.cn/jquery/ajax_ajax.asp)
        $.ajax({
            url: BACKEND,
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false
        }).done(function(res){
            var response = res.response;
            var head     = response.head;
            // 状态检测
            if(head.errcode === '00000') {
                alertify.success('上传成功!');
                clearVM(vm);
                resetRiskList();
                // console.log(response.body.data);
            } else {
                var message = head.message;
                
                if(message && message.length) {

                    $.each(message, function(index, item){
                        // 暂时去hashmap映射
                        try {
                            // alertify.error(hashMap[item.comment].text + ':' + item.validate);
                            alertify.error(item.validate);
                        } catch(e) {}
                    });
                } else {
                    alertify.error('出现未知错误!');
                }

            }
        });
    }

    /**
     * 提交保单
     */
    $('#J_submit').on('click', function(event){
        event.preventDefault();

        // // @todo 表单验证检测
        // if(showTip.nodes.length){
        //     alertify.error("表单输入不正确,请检查!"); 
        //     return; //如果当前页面还有验证没有通过,就不用提交到后台了!
        // }

        submitForm();
    });


    /**
     * 退出系统
     */
    $('#J_logout').on('click', function(e){
        e.preventDefault();

        console.log("***logout***");

            $.ajax({
                url: LOGOUT_POST,
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json;charset=utf-8'
            }).done(function(res){
                 // 状态检测
                if(res.response.head.errcode === '00000') {
                    alertify.success('退出成功!');
                     
                    // 模拟网络延迟来确认Logout API是否处理太快的原因
                    setTimeout(function(){
                    // 重定向
                       location.href = LOGIN;
                    }, 2000);
                } else {
                    // var message = head.message;
                    alertify.error("退出失败!"); 
                }
            });
       
    });

});



