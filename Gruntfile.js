/**
 * Grunt配置文件
 *
 * @date 2017-04-27
 */

module.exports = function(grunt) {

	grunt.initConfig({
		// pkg: grunt.file.readJSON('package.json'),
		
		concat: {
			options: {
		      separator: ';',
		    },

		    /**
		     * 公共模块打包
		     */
		    common: {
		      src: [
		      'vendors/jquery.js',
			  'vendors/bootstrap.3.3.7.js',
			  'mod/validate.js',
			  'vendors/alertify.js',
			  'mod/json2.js',
			  'mod/moment.js'
		      	],
		      dest: 'pack/common.js',
		    },

		    /**
		     * 登录页面模块打包
		     */
		    login: {
		      src: [
		      	'mod/jquery.backstretch.min.js', 
		      	'mod/login.js'
		      	],
		      dest: 'pack/login.js'
		    },

			/**
			* 主页面模块打包
			*/
		    index: {
		    	src: [
					'bundle.js',
					// webuploader相关逻辑打包
					'lib/webuploader.js',
           			'webupload.entry.js'
				],
		    	dest: 'pack/pack.index.js'
		    }
		}

	}); // End initConfig

	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('default', [
		// 打包公共模块
		'concat:common',
		// 打包首页模块
		'concat:index',
		// 打包登陆模块
		'concat:login'
	]);
};
