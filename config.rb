require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "pack"
sass_dir = "styles"
images_dir = "images"
javascripts_dir = "javascripts"

# For Deploy
output_style = :compressed

# For Develop
# output_style = :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


